package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null)
            throw new IllegalArgumentException("arguments may not be null");
        ArrayList temp_x = new ArrayList(x);
        ArrayList temp_y = new ArrayList(y);

        for(int i = 0; i < temp_y.size(); i++){
            if(!temp_x.isEmpty() && temp_x.get(0).equals(temp_y.get(i))){
                temp_x.remove(0);
                temp_y.subList(0, i).clear();
                i = 0;
            }
        }

        if(temp_x.isEmpty())
            return true;

        return false;
    }
}
