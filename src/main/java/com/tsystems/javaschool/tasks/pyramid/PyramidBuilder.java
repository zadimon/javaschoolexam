package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int numbers_count = 0, level_count = 1;
        int l = 0;
        if(inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        while(l < inputNumbers.size()){
            numbers_count++;
            if(numbers_count > level_count){
                numbers_count = 1;
                level_count++;
            }
            l++;
        }
        if(level_count != numbers_count) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        int mas[][] = new int[level_count][numbers_count + (numbers_count - 1)];
        int k = 0;
        l = 1;
        boolean check_zero;
        for(int i = mas.length - 1; i >= 0; i--){
            check_zero = false;
            for(int j = mas[i].length-k -1; j >= k; j--){
                if(check_zero == false){
                    mas[i][j] = inputNumbers.get(inputNumbers.size()-l);
                    check_zero = true;
                    l++;
                }
                else
                    check_zero = false;
            }
            k++;
        }
        return mas;
    }
}
